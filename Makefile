clean:
	@echo "Execute cleaning ..."
	find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
	rm -f coverage.xml

pep8:
	@find . -type f -not -path "*./.venv/*" -name "*.py"|xargs flake8 --max-line-length=120 --ignore=E402 --max-complexity=6


tests: clean pep8
	py.test tests


coverage:clean pep8
	py.test --cov=global_api --cov-report=xml --omit=global_api.views/* tests

sonar: coverage
	sonar-scanner
