db.auth('root', 'admin');
db = db.getSiblingDB('gateway');

db.createCollection("gateway_events",
    {
        capped: true,
        size: 24257514
    },
    autoIndexId = false);
db.getCollection('gateway_events').createIndex({"event": 1});
db.getCollection('gateway_events').createIndex({"msisdn": 1});
db.getCollection('gateway_events').createIndex({'status_code': 1});

db.createUser(
  {
    user: "testUser3",
    pwd: "xyz123",
    roles: [ { role: "read", db: "gateway" } ],
    mechanisms: ["SCRAM-SHA-1", "SCRAM-SHA-256"]
  }
)