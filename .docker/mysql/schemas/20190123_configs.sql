CREATE DATABASE IF NOT EXISTS gateway;

USE gateway;

DROP TABLE IF EXISTS partner;

CREATE TABLE `partner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `default_url`VARCHAR(255) NULL ,
  `actived` BOOLEAN NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

TRUNCATE TABLE gateway.partner;

INSERT INTO gateway.partner (name, default_url, actived) VALUES ('Tim', 'http://globalsdp-mock-homol.whitelabel.com.br/tim_hub', 1);
INSERT INTO gateway.partner (name, default_url, actived) VALUES ('Vivo', null, 1);
INSERT INTO gateway.partner (name, default_url, actived) VALUES ('Algar', null, 1);
INSERT INTO gateway.partner (name, default_url, actived) VALUES ('Oi', null, 1);
INSERT INTO gateway.partner (name, default_url, actived) VALUES ('Claro', null, 1);
INSERT INTO gateway.partner (name, default_url, actived) VALUES ('Nextel', null, 1);
INSERT INTO gateway.partner (name, default_url, actived) VALUES ('Telcel', null, 1);
INSERT INTO gateway.partner (name, default_url, actived) VALUES ('TimHub', 'http://globalsdp-mock-homol.whitelabel.com.br/tim_hub', 1);

DROP TABLE IF EXISTS partner_config;

CREATE TABLE `partner_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_id` int(11) DEFAULT NULL,
  `value` longtext,
  `key` varchar(255) DEFAULT NULL,
  `actived` tinyint(1) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `partner_config_actived_index` (`actived`),
  KEY `partner_config_partner_id_key_index` (`partner_id`,`key`),
  CONSTRAINT `partner_config_partner_id_fk` FOREIGN KEY (`partner_id`) REFERENCES `partner` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;


TRUNCATE TABLE gateway.partner_config;

INSERT INTO gateway.partner_config (partner_id, value, `key`, actived, url) VALUES (1, 'from_backend', 'partner/mt/url', 1, null);
INSERT INTO gateway.partner_config (partner_id, value, `key`, actived, url) VALUES (1, 'from_backend', 'partner/charging/url', 1, null);
INSERT INTO gateway.partner_config (partner_id, value, `key`, actived, url) VALUES (1, 'from_backend', 'partner/charging/url', 1, null);
INSERT INTO gateway.partner_config (partner_id, value, `key`, actived, url) VALUES (1, '[77000,22020]', 'interactivity/la', 1, null);
INSERT INTO gateway.partner_config (partner_id, value, `key`, actived, url) VALUES (1, '["ESPORTE","FSVAS_BELEZA","FSVAS_PIADAS","PENSAMENTOS","CANTADA","CURIOSO","PARA_CHOQUE_DE_CAMINHAO"]', 'interactivity/service_ids', 1, null);
INSERT INTO gateway.partner_config (partner_id, value, `key`, actived, url) VALUES (1, 'to_backend_up', 'interactivity/mo/url', 1, null);
INSERT INTO gateway.partner_config (partner_id, value, `key`, actived, url) VALUES (1, 'to_backend', 'tim/hub/v1/backend/mo/url', 1, 'http://globalsdp-mock-homol.whitelabel.com.br/tim_hub');
INSERT INTO gateway.partner_config (partner_id, value, `key`, actived, url) VALUES (1, '[77000,22020]', 'tim/hub/v1/interactivity/la', 1, null);


