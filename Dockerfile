FROM python:3.7.2-alpine

ARG ENVIRONMENT='Production'
ARG DEPLOY_PATH='/opt/global_api'

ENV PYTHONUNBUFFERED=true
ENV C_FORCE_ROOT=true
ENV ENVIRONMENT=$ENVIRONMENT

ENV APPLICATION_DEBUG=false
ENV APPLICATION_PORT=8888
ENV CELERY_BROKER_URL='amqp://guest:guest@gateway.rabbitmq:5672'
ENV CELERY_ALWAYS_EAGER=false
ENV CELERY_MAX_RETRY_RETRIES=3
ENV CELERY_DEFAULT_RETRY_DELAY=1
ENV CELERY_DISABLE_RATE_LIMITS=false
ENV CELERYD_CONCURRENCY=20
ENV CELERYD_PREFETCH_MULTIPLIER=30
ENV CELERY_DEFAULT_EXCHANGE_TYPE='direct'
ENV CELERY_IGNORE_RESULT=true
ENV CELERY_ACKS_LATE=true
ENV CELERY_DEFAULT_RETRY_DELAY=5
ENV SQL_HOST='gateway.mysql'
ENV SQL_USER='root'
ENV SQL_PASSWORD='admin'
ENV SQL_PORT=3306
ENV SQL_DATABASE='gateway'
ENV NO_SQL_HOST='gateway.mongodb1'
ENV NO_SQL_PORT=27017
ENV NO_SQL_PASSWORD='admin'
ENV NO_SQL_USER='root'
ENV NO_SQL_DATABASE='gateway'
ENV REDIS_HOST='gateway.redis'
ENV REDIS_PORT=6379
ENV REDIS_TPS='gateway.redis://redis:6379/0'
ENV REDIS_PREBILLING='gateway.redis://redis:6379/2'
ENV REDIS_REQUEST_ID='gateway.redis://redis:6379/3'
ENV REDIS_DB_PREBILLING=0
ENV REDIS_TPS=2
ENV REDIS_DB_REQUEST_ID=3
ENV REDIS_CACHE_EXPIRES=999
ENV LEVEL_LOG='INFO'

RUN apk update \
  && apk add \
    build-base \
    libpq \
    libffi-dev \
    supervisor \
    openssh-client \
    git \
    mariadb-dev \
    linux-headers \
    alpine-sdk \
    openrc --no-cache

WORKDIR /opt/global_api
RUN pip install -U pip
RUN pip install pipenv

COPY ./Pipfile /opt/global_api
COPY ./Pipfile.lock /opt/global_api
COPY ./main.py /opt/global_api/
COPY ./supervisor.conf /etc/supervisor/conf.d/

RUN pipenv install --skip-lock


EXPOSE 80

ENTRYPOINT ["supervisord", "-c", "/etc/supervisor/conf.d/supervisor.conf"]
