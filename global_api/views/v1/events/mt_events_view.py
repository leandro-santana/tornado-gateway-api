import json
from http import HTTPStatus

from tornado.httpclient import HTTPError
from webargs.tornadoparser import parser

from global_api.exceptions import GW_ERRORS
from global_api.integrations.base.base_factory import BaseDefaultFactory
from global_api.models.nosql import NoSqlDriver
from global_api.models.nosql.gateway_events_collection import GatewayEvents
from global_api.views import ApiJsonHandler


class MtEventsView(ApiJsonHandler):
    event = 'mt'

    async def post(self, *args, **kwargs):
        carrier = json.loads(self.request.body).get('carrier')
        queue = '{carrier}_{queue}'.format(carrier=carrier, queue=self.event)
        try:
            headers = dict(self.request.headers)
            instance = BaseDefaultFactory.get_instance(carrier=carrier, event=self.event, headers=headers)

            payload = json.dumps(parser.parse(instance.contract(), self.request, locations=('json',)))
            response = await instance.process(payload=payload, obj_request=self.request,
                                              queue_mode=True, method='POST', queue=queue)
            no_sql_driver = NoSqlDriver()
            no_sql_driver.db_save(GatewayEvents, request=json.loads(payload), response=response, header=headers,
                                  msisdn=instance.msisdn, partner=carrier, event=self.event, host=self.request.host,
                                  status_code=HTTPStatus.OK.value)

            self.success(code=HTTPStatus.OK.value, message=response)
        except HTTPError as error:
            self.error(code=error.code, message=error.message)
        except GW_ERRORS as error:
            self.error(code=error.code, message=error.message)
