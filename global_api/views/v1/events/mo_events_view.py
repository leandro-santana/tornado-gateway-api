import json
from http import HTTPStatus

from tornado.httpclient import HTTPError
from webargs.tornadoparser import parser

from global_api.exceptions import GW_ERRORS
from global_api.integrations.base.base_factory import BaseDefaultFactory
from global_api.models.nosql import NoSqlDriver
from global_api.models.nosql.gateway_events_collection import GatewayEvents
from global_api.views import ApiJsonHandler


class MoEventsView(ApiJsonHandler):
    event = 'mo'

    async def post(self, *args, **kwargs):
        carrier = kwargs.get('carrier')
        queue = '{carrier}_{queue}'.format(carrier=carrier, queue=self.event)
        try:
            headers = dict(self.request.headers)
            instance = BaseDefaultFactory.get_instance(carrier=carrier, event=self.event, headers=headers)
            payload = json.dumps(parser.parse(instance.contract(), self.request, locations=('json',)))
            response_process = await instance.process(payload=payload, queue_mode=True, method='POST', queue=queue)
            response = {'status_code': HTTPStatus.OK.value, 'response': response_process}

            NoSqlDriver().db_save(GatewayEvents, request=json.loads(payload), response=response, header=headers,
                                  msisdn=instance.msisdn, partner=carrier, event=self.event, host=self.request.host,
                                  status_code=HTTPStatus.OK.value)

            self.success(code=HTTPStatus.OK.value, message=response_process)
        except HTTPError as error:
            self.error(code=error.code, message=error.message)
        except GW_ERRORS as error:
            self.error(code=error.code, message=error.message)
