from healthcheck import HealthCheck
from kombu import Connection

from global_api.configs import config
from global_api.models.nosql import NoSqlDriver
from global_api.models.sql import DBDriver
from global_api.services.cache_driver import CacheDriver
from global_api.views import ApiJsonHandler


class HealthcheckApi(ApiJsonHandler):
    @staticmethod
    def __check_rabbitmq():
        broker_url = config.BROKER_URL
        connection = Connection(broker_url)
        connection.ensure_connection(max_retries=1)
        return True, "rabbitmq ok"

    @staticmethod
    def __check_mysql():
        db_driver = DBDriver()
        db_driver.db_session.query("1").from_statement("SELECT 1").all()
        return True, "mysql ok"

    @staticmethod
    def __check_redis():
        cache1 = CacheDriver(p_host=config.REDIS.get('host'),
                             p_port=config.REDIS.get('port'),
                             p_db=config.REDIS_DB_REQUEST_ID)
        cache1.get(key=123)

        cache2 = CacheDriver(p_host=config.REDIS.get('host'),
                             p_port=config.REDIS.get('port'),
                             p_db=config.REDIS_TPS)
        cache2.get(key=123)

        cache3 = CacheDriver(p_host=config.REDIS.get('host'),
                             p_port=config.REDIS.get('port'),
                             p_db=config.REDIS_DB_PREBILLING)
        cache3.get(key=123)

        return True, "redis ok"

    @staticmethod
    def __check_mongo():
        NoSqlDriver(config.MONGO_CONNECT)
        return True, "mongodb ok"

    def get(self, *args, **kwargs):
        health = HealthCheck()

        health.add_check(self.__check_rabbitmq)
        health.add_check(self.__check_mysql)
        health.add_check(self.__check_redis)
        health.add_check(self.__check_mongo)

        message, status_code, headers = health.run()
        self.set_status(status_code)
        self.finish(message)
