from sqlalchemy import Column, ForeignKey, Index, String
from sqlalchemy.dialects.mysql import INTEGER, LONGTEXT, TINYINT
from sqlalchemy.orm import relationship

from global_api.models.sql.model_base import ModelBase
from global_api.models.sql.tb_partner import Partner


class PartnerConfig(ModelBase):
    __tablename__ = 'partner_config'
    __table_args__ = (
        Index('partner_config_partner_id_key_index', 'partner_id', 'key'),
    )

    id = Column(INTEGER(11), primary_key=True)
    partner_id = Column(INTEGER(11), ForeignKey(Partner.id))
    value = Column(LONGTEXT)
    key = Column(String(255))
    url = Column(String(255))
    actived = Column(TINYINT(1), index=True)

    partner = relationship(Partner, foreign_keys=[partner_id])

    def get_settings(self):
        return self.orm.db_session.query(Partner, PartnerConfig).filter(Partner.id == PartnerConfig.partner_id,
                                                                        Partner.actived == 1,
                                                                        PartnerConfig.actived == 1).all()
