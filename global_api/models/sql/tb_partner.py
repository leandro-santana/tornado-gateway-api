from sqlalchemy import Column, String, text
from sqlalchemy.dialects.mysql import INTEGER, TINYINT

from global_api.models.sql.model_base import ModelBase


class Partner(ModelBase):
    __tablename__ = 'partner'

    id = Column(INTEGER(11), primary_key=True)
    name = Column(String(45))
    default_url = Column(String(255))
    actived = Column(TINYINT(1), server_default=text("'0'"))
