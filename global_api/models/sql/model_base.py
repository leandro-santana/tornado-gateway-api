from sqlalchemy.ext.declarative import declarative_base

from global_api.models.sql import DBDriver

Base = declarative_base()


class ModelBase(Base):
    __abstract__ = True

    @property
    def orm(self):
        return DBDriver()
