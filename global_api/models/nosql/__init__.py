from pymodm import connect

from global_api.configs import config, gw_logger
from global_api.services import Singleton


class NoSqlDriver(metaclass=Singleton):
    """ Nosql driver"""

    def __init__(self, host=config.MONGO_CONNECT):
        self.__connection = connect(host)

    def db_save(self, model, **kwargs):
        try:
            model = model()
            for field, value in kwargs.items():
                setattr(model, field, value)
            return model.save()
        except Exception as error:
            gw_logger.get().exception(str(error))
