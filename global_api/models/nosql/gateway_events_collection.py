import datetime

from pymodm import MongoModel, fields
from pymongo import IndexModel, ASCENDING


class GatewayEvents(MongoModel):
    partner = fields.CharField(required=True, max_length=50)
    event = fields.CharField(required=True, max_length=50)
    header = fields.DictField(required=True)
    msisdn = fields.IntegerField(required=True)
    host = fields.CharField(required=True, max_length=250)
    status_code = fields.IntegerField(required=True)
    request = fields.DictField(required=True)
    response = fields.DictField(required=True)
    created_at = fields.DateTimeField(default=datetime.datetime.now())

    class Meta:
        indexes = [
            IndexModel([
                ('event', ASCENDING)
            ]),
            IndexModel([
                ('msisdn', ASCENDING)
            ]),
            IndexModel([
                ('partner', ASCENDING)
            ]),
            IndexModel([
                ('status_code', ASCENDING)
            ])
        ]
