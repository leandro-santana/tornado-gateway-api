from tornado.web import Application

from global_api.urls import routes


def create_app():
    app = Application(routes)
    return app
