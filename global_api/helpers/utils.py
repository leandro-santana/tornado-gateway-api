def clear_none_values_in_dict(dict_value):
    return {k: v for k, v in dict_value.items() if v is not None}
