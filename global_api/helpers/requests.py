import json

from tornado.httpclient import AsyncHTTPClient, HTTPRequest, HTTPClient


class Requests(object):
    http_client = None

    def __init__(self, url: str, header: dict, body: dict, method: str = "POST", **kwargs):
        self.url = url
        self.header = header
        self.body = json.dumps(body)
        self.method = method
        self.kwargs = kwargs

    def __request(self):
        response = self.http_client.fetch(
            HTTPRequest(url=self.url, method=self.method, headers=self.header, body=self.body,
                        validate_cert=False, request_timeout=30))
        return response

    async def call_request_async(self):
        """  tornado function to async requests """
        self.http_client = AsyncHTTPClient()
        response = await self.__request()
        return response

    def call_request_sync(self):
        """  tornado function to sync requests """
        self.http_client = HTTPClient()
        response = self.__request()
        return response
