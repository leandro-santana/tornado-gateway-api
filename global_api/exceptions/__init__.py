from http import HTTPStatus


class GenericError(Exception):
    code = 500
    message = 'Internal Server Error'

    def __init__(self, message=None):
        if message:
            self.message = message


class InvalidConnection(GenericError):
    def __init__(self, message, code=HTTPStatus.BAD_REQUEST.value):
        super(InvalidConnection, self).__init__(message)
        self.code = code


class InvalidInstance(GenericError):
    def __init__(self, message, code=HTTPStatus.NOT_FOUND.value):
        super(InvalidInstance, self).__init__(message)
        self.code = code


class InvalidEntity(GenericError):
    def __init__(self, message, code=HTTPStatus.UNPROCESSABLE_ENTITY.value):
        super(InvalidEntity, self).__init__(message)
        self.code = code


GW_ERRORS = (InvalidEntity, InvalidConnection, InvalidInstance)
