import logging
import logging.config
import os
import sys
from contextvars import ContextVar

from kombu import Queue, Exchange


class Production(object):
    APP_PORT = 80
    CELERY_SERIALIZATION = 'json'
    CELERY_TIMEZONE = 'UTC'
    CELERY_MAX_RETRY_RETRIES = os.environ.get('CELERY_MAX_RETRY_RETRIES')
    CELERY_DEFAULT_RETRY_DELAY = os.environ.get('CELERY_DEFAULT_RETRY_DELAY')
    CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'msgpack', 'yaml']
    BROKER_URL = os.getenv('CELERY_BROKER_URL')
    CELERY_DISABLE_RATE_LIMITS = os.getenv('CELERY_DISABLE_RATE_LIMITS')
    CELERYD_CONCURRENCY = os.getenv('CELERYD_CONCURRENCY')
    CELERY_CREATE_MISSING_QUEUES = True
    CELERY_DEFAULT_EXCHANGE_TYPE = os.getenv('CELERY_DEFAULT_EXCHANGE_TYPE')
    CELERY_IGNORE_RESULT = os.getenv('CELERY_IGNORE_RESULT')
    CELERY_ACKS_LATE = os.getenv('CELERY_ACKS_LATE')
    CELERYD_PREFETCH_MULTIPLIER = os.getenv('CELERYD_PREFETCH_MULTIPLIER')
    CELERY_QUEUES = (
        Queue('tim_mt_queue', exchange=Exchange('tim_mt', type='direct'),
              routing_key='rk:tim_mt',
              binding_key='tim_mt', queue_arguments={'x-max-priority': 10}),
    )
    GENERIC_QUEUE = 'generic_queue'

    SQL_USER = os.getenv('SQL_USER')
    SQL_PASSWORD = os.getenv('SQL_PASSWORD')
    SQL_HOST = os.getenv('SQL_HOST')
    SQL_DATABASE = os.getenv('SQL_DATABASE')
    SQL_PORT = os.getenv('SQL_PORT')

    MYSQL_CONNECT = 'mysql+pymysql://{user}:{password}@{host}/{db}'.format(user=SQL_USER, password=SQL_PASSWORD,
                                                                           host=SQL_HOST, db=SQL_DATABASE)

    NO_SQL_HOST = os.getenv('NO_SQL_HOST')
    NO_SQL_PORT = os.getenv('NO_SQL_PORT')
    NO_SQL_PASSWORD = os.getenv('NO_SQL_PASSWORD')
    NO_SQL_USER = os.getenv('NO_SQL_USER')
    NO_SQL_DATABASE = os.getenv('NO_SQL_DATABASE')

    MONGO_CONNECT = 'mongodb://{user}:{password}@{host}:{port}/{database}?authSource={level}'. \
        format(user=NO_SQL_USER, password=NO_SQL_PASSWORD,
               database=NO_SQL_DATABASE, host=NO_SQL_HOST,
               port=NO_SQL_PORT, level='admin')

    REDIS = {'host': os.environ.get('REDIS_HOST'), 'port': os.environ.get('REDIS_PORT')}

    REDIS_DB_PREBILLING = os.environ.get('REDIS_TPS')
    REDIS_TPS = os.environ.get('REDIS_DB_PREBILLING')
    REDIS_DB_REQUEST_ID = os.environ.get('REDIS_DB_REQUEST_ID')

    REDIS_CACHE_EXPIRES = os.environ.get('REDIS_CACHE_EXPIRES', 20)

    APP_DEBUG = os.environ.get('APP_DEBUG', False)
    PATH_LOG = '/fs/logs/global_api'
    LEVEL_LOG = 'INFO'

    TIMEOUT_RECONNECT_MYSQL = 3000
    POLL_SIZE_MYSQL = 20
    HTTP_CODES_TO_RETRY = list(set(range(500, 512)) | {429})
    QUEUED_MESSAGE = 'queued_success'


class Development(Production):
    APP_PORT = 8888
    CELERY_MAX_RETRY_RETRIES = 3
    CELERY_ALWAYS_EAGER = False
    CELERY_DEFAULT_RETRY_DELAY = 1
    BROKER_URL = 'amqp://guest:guest@localhost:5672/'
    CELERY_DISABLE_RATE_LIMITS = False
    CELERYD_CONCURRENCY = 20
    CELERYD_PREFETCH_MULTIPLIER = 30
    CELERY_DEFAULT_EXCHANGE_TYPE = 'direct'
    CELERY_IGNORE_RESULT = True
    CELERY_ACKS_LATE = True
    SQL_USER = 'root'
    SQL_PASSWORD = 'admin'
    SQL_HOST = 'localhost'
    SQL_DATABASE = 'gateway'
    NO_SQL_HOST = 'localhost'
    NO_SQL_PORT = 27017
    NO_SQL_PASSWORD = 'admin'
    NO_SQL_USER = 'root'
    NO_SQL_DATABASE = 'gateway'
    REDIS = {'host': 'redis', 'port': '6379'}
    REDIS_DB_PREBILLING = 0
    REDIS_TPS = 2
    REDIS_DB_REQUEST_ID = 3
    REDIS_CACHE_EXPIRES = 999
    APP_DEBUG = True
    LEVEL_LOG = 'DEBUG'
    MYSQL_CONNECT = 'mysql+pymysql://{user}:{password}@{host}/{db}'.format(user=SQL_USER, password=SQL_PASSWORD,
                                                                           host=SQL_HOST, db=SQL_DATABASE)

    MONGO_CONNECT = 'mongodb://{user}:{password}@{host}:{port}/{database}?authSource={level}'. \
        format(user=NO_SQL_USER, password=NO_SQL_PASSWORD,
               database=NO_SQL_DATABASE, host=NO_SQL_HOST,
               port=NO_SQL_PORT, level='admin')


class Testing(Production):
    APP_DEBUG = True


def _load_config():
    _config_class = getattr(sys.modules[__name__], str(os.getenv('ENVIRONMENT', 'Development')))
    return _config_class()


config = _load_config()


class ConfigLog(object):
    @property
    def logger(self):
        logging.config.dictConfig({
            'version': 1,
            'disable_existing_loggers': False,
            'formatters': {
                "simple": {
                    "format": "%(asctime)s - [%(levelname)s] - [%(process)d] - [%(filename)s:%(lineno)s] - %(message)s",
                    'datefmt': '%Y-%m-%d %H:%M:%S'
                },
                'globalsdp-format': {
                    'format': '%(message)s'
                }
            },
            'handlers': {
                "global_api": {
                    "class": "logging.handlers.TimedRotatingFileHandler",
                    "when": 'midnight',
                    'formatter': 'globalsdp-format',
                    "filename": "{path}/global_api.log".format(path=config.PATH_LOG)
                },
                'console': {
                    'class': 'logging.StreamHandler',
                    'formatter': 'simple',
                    'stream': 'ext://sys.stdout'
                },
                'tornado.access': {
                    'level': 'INFO',
                    'formatter': 'simple',
                    'class': 'logging.StreamHandler',
                    'stream': 'ext://sys.stdout'
                }
            },
            'loggers': {
                'global_api': {
                    'handlers': ['global_api', 'console'],
                    'level': '{level}'.format(level=str(config.LEVEL_LOG)),
                    'propagate': False
                },
                'tornado.access': {
                    'handlers': ['tornado.access'],
                    'level': 'INFO',
                    'propagate': True
                }
            }})
        return logging.getLogger('global_api')


gw_logger = ContextVar('gw_logger')
gw_logger.set(ConfigLog().logger)
