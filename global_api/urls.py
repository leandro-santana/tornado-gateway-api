from global_api.views.healthcheck import HealthcheckApi
from global_api.views.v1.events.mo_events_view import MoEventsView
from global_api.views.v1.events.mt_events_view import MtEventsView

routes = [
    (r'/api/healthcheck', HealthcheckApi),
    (r'/(?P<carrier>\w+)(?P<hub>/hub)?/mo', MoEventsView),
    (r'/api/v1/mt', MtEventsView)
]
