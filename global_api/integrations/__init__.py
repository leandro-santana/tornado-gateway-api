import json

from global_api.configs import config
from global_api.exceptions import InvalidConnection
from global_api.helpers.requests import Requests
from global_api.services.gateway import GwRoutes
from global_api.services.queue_driver import QueueDriver


class IntegrationsMixin(object):
    """ Integrations class, use to instance in class to shared options"""

    msisdn = None
    partner = None
    key = None
    event = None
    _header = None

    async def process_in_queue_mode(self, request, **kwargs):
        """ request is send to queue """
        gw_routes = GwRoutes()
        url = gw_routes.open().get(self.partner, {}).get(self.key)
        queue = kwargs.get('queue', config.GENERIC_QUEUE)
        message = {
            'message': {
                'url': url,
                'msisdn': self.msisdn,
                'request': request,
                'headers': self._header,
                'method': kwargs.get('method', 'POST'),
                'partner': self.partner,
                'event': self.event
            }
        }
        queue_driver = QueueDriver('worker_job_request')
        queue_id = await queue_driver.send_to_queue(message=message, queue=queue, priority=1)

        if not queue_id:
            raise InvalidConnection(message='queued_error')
        response = {config.QUEUED_MESSAGE: str(queue_id)}
        return response

    async def process_in_request_mode(self, request, **kwargs):
        """ request is send to url """
        gw_routes = GwRoutes()
        url = gw_routes.open().get(self.partner, {}).get(self.key)

        send_request = Requests(url=url,
                                header=self._header,
                                method=kwargs.get('method', 'POST'),
                                event=self.event,
                                body=request)
        response = await send_request.call_request_async()
        response = json.loads(response.body.decode())
        response = {'response': response}
        return response

    async def process(self, payload, **kwargs):
        """ process request """
        request = json.loads(payload)
        request = await self.normalize_request(request)

        mode = self.process_in_request_mode
        if kwargs.get('queue_mode', False):
            mode = self.process_in_queue_mode

        return await mode(request, **kwargs)
