from webargs import fields

from global_api.helpers.utils import clear_none_values_in_dict
from global_api.integrations import IntegrationsMixin
from global_api.integrations.base.base_default import BaseDefault


class TimMo(IntegrationsMixin, BaseDefault):
    carrier = 'tim'
    event = 'mo'

    def __init__(self, headers: dict = {}):
        partner, key = 'tim', 'tim/hub/v1/backend/mo/url'
        super(TimMo, self).__init__(partner=partner, key=key, headers=headers, authorization='')

    def contract(self):
        """ communication contract to TIM MO"""
        contract = {
            "SmsNotificationRequest": fields.Nested(
                {
                    "shortNumber": fields.Int(required=True),
                    "txId": fields.Str(required=True),
                    "msisdn": fields.Int(required=True),
                    "text": fields.Str(required=True),
                    "token": fields.Str(required=True),
                }, unknown="EXCLUDE", required=True)
        }
        return contract

    async def normalize_request(self, origin_request: dict = {}):
        """ normalizing request to send to destination"""
        self.msisdn = origin_request.get('SmsNotificationRequest', {}).get('msisdn')
        effective_request = {
            "shortNumber": origin_request.get('SmsNotificationRequest', {}).get('shortNumber'),
            "txId": origin_request.get('SmsNotificationRequest', {}).get('txId'),
            "msisdn": self.msisdn,
            "text": origin_request.get('SmsNotificationRequest', {}).get('text'),
            "token": origin_request.get('SmsNotificationRequest', {}).get('token'),
        }

        request = clear_none_values_in_dict(effective_request)

        return request
