from webargs import fields

from global_api.helpers.utils import clear_none_values_in_dict
from global_api.integrations import IntegrationsMixin
from global_api.integrations.base.base_default import BaseDefault


class TimMt(IntegrationsMixin, BaseDefault):
    carrier = 'tim'
    event = 'mt'

    def __init__(self, headers: dict = {}):
        partner, key = 'tim', 'partner/mt/url'
        super(TimMt, self).__init__(partner=partner, key=key, headers=headers, authorization='')

    def contract(self):
        """ communication contract to TIM MT"""
        contract = {
            "msisdn": fields.Integer(required=True),
            "la": fields.Integer(required=False),
            "text": fields.Str(required=True),
            "txid": fields.Str(required=True),
            "service_id": fields.Str(required=False),
            "carrier": fields.Str(required=True)
        }
        return contract

    async def normalize_request(self, origin_request: dict = {}):
        """ normalizing request to send to destination"""
        self.msisdn = origin_request.get('msisdn')
        effective_request = {
            "SendSmsRequest": clear_none_values_in_dict({
                "txId": origin_request.get('txid'),
                "serviceId": origin_request.get('service_id'),
                "subscriptionId": origin_request.get('subscriptionId'),
                "token": origin_request.get('token'),
                "msisdn": self.msisdn,
                "text": origin_request.get('text'),
                "ttlMinutes": origin_request.get('ttlMinutes')
            })
        }
        request = clear_none_values_in_dict(effective_request)

        return request
