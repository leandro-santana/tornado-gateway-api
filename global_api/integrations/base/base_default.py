import uuid
from abc import ABCMeta, abstractmethod


class BaseDefault(metaclass=ABCMeta):
    """
    standard basis for implementation, uses in classes such as mt and mo
    json of communication
    """

    def __init__(self, partner: str = '', key: str = '', headers: dict = {}, authorization: str = '', **kwargs):
        self.msisdn = kwargs.get('msisdn', 0)
        self.partner = partner
        self.key = key
        self._header = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': authorization,
            'X-Fs-Trackid': headers.get('X-Fs-Trackid', 'global-{}'.format(uuid.uuid4()))
        }

    @abstractmethod
    def process(self, request):
        raise NotImplementedError("Implement me")

    @abstractmethod
    def normalize_request(self, origin_request: dict = {}):
        raise NotImplementedError("Implement me")

    @abstractmethod
    def contract(self):
        raise NotImplementedError("Implement me")
