from http import HTTPStatus

from tornado.web import HTTPError

from global_api.integrations.base.base_default import BaseDefault
from global_api.integrations.tim.input.mo import TimMo
from global_api.integrations.tim.output.mt import TimMt

__all__ = ['TimMt', 'TimMo']
LIST_ABSTRACTS = [BaseDefault]


class BaseDefaultFactory:
    @staticmethod
    def get_instance(carrier, event, headers: dict = {}):
        for abstracts_classes in LIST_ABSTRACTS:
            for klass in abstracts_classes.__subclasses__():
                if (carrier.lower() == klass.carrier) and (event.lower() == klass.event):
                    return klass(headers=headers)
        raise HTTPError(status_code=HTTPStatus.NOT_FOUND.value, reason='Carrier not found')
