from global_api.configs import config
from global_api.models.sql.tb_partner_config import PartnerConfig

from global_api.services import Singleton
from global_api.services.cache_driver import CacheDriver

routes = {}


class GwRoutes(metaclass=Singleton):
    routes = {}

    def open(self):
        configs_key = 'configs_routes'
        cache = CacheDriver(p_host=config.REDIS.get('host'),
                            p_port=config.REDIS.get('port'),
                            p_db=config.REDIS_DB_REQUEST_ID)

        if not cache.get(key=configs_key) or not len(self.routes):
            actived_settings = PartnerConfig().get_settings()
            for settings in actived_settings:
                partner = settings[0].name.lower()
                if not self.routes.get(partner):
                    self.routes.update({partner: {}})
                if 'url' in settings[1].key:
                    set_url = settings[1].url
                    if not settings[1].url:
                        set_url = settings[0].default_url
                    self.routes[partner].update(
                        {settings[1].key: '{url}/{link}'.format(url=set_url, link=settings[1].value)})
                    continue
                self.routes[partner].update({settings[1].key: settings[1].value})
            cache.set(configs_key, 1)
        return self.routes
