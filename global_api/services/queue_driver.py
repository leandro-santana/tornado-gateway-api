from global_api.services import Singleton
from global_api.tasks.gateway import worker_job_request


class QueueDriver(metaclass=Singleton):
    list_jobs_queues = {'worker_job_request': worker_job_request}

    def __init__(self, job_queue):
        job = self.list_jobs_queues.get(job_queue)
        if not job:
            raise Exception('has not queue {queue_job}'.format(queue_job=job_queue))

        self.__job_queue = self.list_jobs_queues.get(job_queue)

    async def send_to_queue(self, message, **kwargs):
        queue_id = self.__job_queue.apply_async(
            kwargs=message, queue=kwargs.get('queue'),
            priority=int(kwargs.get('priority')))
        return queue_id
