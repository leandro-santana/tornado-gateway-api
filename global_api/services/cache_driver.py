import redis

from global_api.configs import config
from global_api.services import SingletonComplexity


class CacheDriver(metaclass=SingletonComplexity):
    """Class the of access redis."""

    def __init__(self, p_host, p_port, p_db):
        self.__redis = redis.StrictRedis(host=p_host, port=int(p_port), db=int(p_db))

    def set(self, key, value, ttl: int = config.REDIS_CACHE_EXPIRES):
        self.__redis.set(key, value, ttl)

    def get(self, key):
        value = self.__redis.get(key)
        if value:
            value = value.decode()
        return value

    def delete(self, key):
        self.__redis.delete(key)

    def flushall(self):
        self.__redis.flushall()
