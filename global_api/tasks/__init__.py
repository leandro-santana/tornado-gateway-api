from celery import Celery

from global_api.configs import config

app = Celery(include=['global_api.tasks.gateway'])

app.config_from_object(config)
app.conf.task_queue_max_priority = 10
