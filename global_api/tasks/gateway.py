import json
from http import HTTPStatus

from celery.exceptions import MaxRetriesExceededError
from tornado.httpclient import HTTPError

from global_api.configs import config, gw_logger
from global_api.helpers.requests import Requests
from global_api.models.nosql import NoSqlDriver
from global_api.models.nosql.gateway_events_collection import GatewayEvents
from global_api.tasks import app


@app.task(bind=True, autoretry_for=(Exception, HTTPError),
          retry_kwargs={'max_retries': config.CELERY_MAX_RETRY_RETRIES, "countdown": config.CELERY_DEFAULT_RETRY_DELAY})
def worker_job_request(self, **kwargs):
    no_sql_driver = NoSqlDriver()
    try:

        message = kwargs.get('message')
        requests = Requests(url=message.get('url'), method=message.get('method'),
                            header=message.get('headers'), body=message.get('request'))
        response = requests.call_request_sync()
        response = json.loads(response.body.decode())

        no_sql_driver.db_save(GatewayEvents, request=message.get('request'), response=response,
                              msisdn=message.get('msisdn', 0), header=message.get('headers'),
                              partner=message.get('partner'), event=message.get('event'), host=message.get('url'),
                              status_code=HTTPStatus.OK.value)

    except HTTPError as error:
        retry_count = self.request.retries + 1
        if self.max_retries > retry_count and error.code in config.HTTP_CODES_TO_RETRY:
            raise MaxRetriesExceededError(error)

        response = {'code': error.code, 'message': error.message}
        no_sql_driver.db_save(GatewayEvents, request=message.get('request'), response=response,
                              msisdn=message.get('msisdn', 0), header=message.get('headers'),
                              partner=message.get('partner'), event=message.get('event'), host=message.get('url'),
                              status_code=error.code)
    except Exception as error:
        gw_logger.get().exception(str(error))
