# Tornado Gateway API

**Docker**<br />
Contains services needed to start GlobalSDP and others that help
development and debuggin
```
$ docker-compose up
```

Install and activate virtualenv with version ```Python 3.7.2``` </br>
Install libs with pipenv install, follow command below.

```
$ pip install pipenv
$ pipenv sync --dev #for develop environment
```


**ENVs**<br />
Below, all Envs that can be changed, with their respective default values.

```
SQL_USER='root'
SQL_PASSWORD='admin'
SQL_HOST='gateway.mysql'
SQL_DATABASE='gateway'
SQL_PORT=3306
REDIS_TPS='redis://redis:6379/0'
REDIS_PRE_BILLING='redis://redis:6379/2'
REDIS_REQUEST_ID='redis://redis:6379/3'
REDIS_CACHE_EXPIRES = 1000
APP_DEBUG='True'
PORT=8888
LEVEL_LOG="DEBUG"
```

**Running API Server**
```
$ main.py
```

**Running the workers**
```
$ celery -A global_api.tasks:app worker -C 1 -Q tim_mt,tim_mo
```

**Running Sonarqube**
```
$ docker run -d --name sonarqube -p 9000:9000 sonarqube:7.5-community
make sonar
```
You can access the reports by accessing http://localhost:9000/

**Running tests**<br />
To test workers, execute the command below:
```
make tests
```
