from global_api.exceptions import InvalidConnection, InvalidInstance, InvalidEntity
from tests import BaseTests


class TestApiExceptions(BaseTests):
    def test_invalid_connection(self):
        message = 'test_invalid_connection'
        invalid_connection = InvalidConnection(message=message)

        self.assertEqual(400, invalid_connection.code)
        self.assertEqual(message, invalid_connection.message)

    def test_invalid_instance(self):
        message = 'test_invalid_instance'
        invalid_connection = InvalidInstance(message=message)

        self.assertEqual(404, invalid_connection.code)
        self.assertEqual(message, invalid_connection.message)

    def test_invalid_entity(self):
        message = 'test_invalid_entity'
        invalid_connection = InvalidEntity(message=message)

        self.assertEqual(422, invalid_connection.code)
        self.assertEqual(message, invalid_connection.message)
