import mock
from celery.exceptions import MaxRetriesExceededError
from tornado.httpclient import HTTPError

from global_api.tasks.gateway import worker_job_request
from tests import BaseTests


class ResponseMock123(object):
    body = b'123456'


class TestGateway(BaseTests):
    @mock.patch('global_api.tasks.gateway.Requests')
    @mock.patch('global_api.tasks.gateway.NoSqlDriver')
    def test_task_message_success(self, mock_sql_driver, mock_requests):
        mock_requests().call_request_sync = mock.MagicMock(return_value=ResponseMock123)
        worker_job_request(message={})
        self.assertTrue(mock_sql_driver.called)
        self.assertTrue(mock_requests.called)

    @mock.patch('global_api.tasks.gateway.Requests')
    @mock.patch('global_api.tasks.gateway.NoSqlDriver')
    def test_task_message_with_http_error_exception_and_not_retries(self, mock_sql_driver, mock_requests):
        mock_requests().call_request_sync = mock.MagicMock(side_effect=HTTPError(401, 'my_error'))
        worker_job_request(message={})
        self.assertTrue(mock_sql_driver.called)
        self.assertTrue(mock_requests.called)

    @mock.patch('global_api.tasks.gateway.Requests')
    @mock.patch('global_api.tasks.gateway.NoSqlDriver')
    def test_task_message_with_http_error_exception_and_retries_true(self, mock_sql_driver, mock_requests):
        mock_requests().call_request_sync = mock.MagicMock(side_effect=HTTPError(500, 'my_error'))
        with self.assertRaises(MaxRetriesExceededError):
            worker_job_request(message={})
        self.assertTrue(mock_sql_driver.called)
        self.assertTrue(mock_requests.called)

    @mock.patch('global_api.tasks.gateway.Requests')
    @mock.patch('global_api.tasks.gateway.NoSqlDriver')
    @mock.patch('global_api.tasks.gateway.gw_logger')
    def test_task_message_with_http_error_exception(self, mock_gw_logger, mock_sql_driver,
                                                    mock_requests):
        mock_requests().call_request_sync = mock.MagicMock(side_effect=Exception())
        worker_job_request(message={})
        self.assertTrue(mock_sql_driver.called)
        self.assertTrue(mock_requests.called)
        self.assertTrue(mock_gw_logger.get.called)
