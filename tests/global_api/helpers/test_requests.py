import mock
from tornado.testing import gen_test

from global_api.helpers.requests import Requests
from tests import BaseTestsAsync


class TestRequests(BaseTestsAsync):
    @mock.patch('global_api.helpers.requests.AsyncHTTPClient')
    @mock.patch('global_api.helpers.requests.HTTPRequest')
    @gen_test
    async def test_call_request_async(self, mock_request, mock_async_http_client):
        requests = Requests(url='http://my-uri.com',
                            header={"Content-Type": "application/json"},
                            body={"body": "nothing"},
                            method="POST")
        await requests.call_request_async()
        self.assertTrue(mock_request.called)
        self.assertTrue(mock_async_http_client.called)

    @mock.patch('global_api.helpers.requests.HTTPClient')
    @mock.patch('global_api.helpers.requests.HTTPRequest')
    def test_call_request_sync(self, mock_request, mock_http_client):
        requests = Requests(url='http://my-uri.com',
                            header={"Content-Type": "application/json"},
                            body={"body": "nothing"},
                            method="POST")
        requests.call_request_sync()
        self.assertTrue(mock_request.called)
        self.assertTrue(mock_http_client.called)
