import mock

from global_api.models.sql.model_base import ModelBase
from tests import BaseTests


class TestModelBase(BaseTests):
    @mock.patch('global_api.models.sql.model_base.DBDriver')
    def test_instance_orm(self, mock_db_driver):
        model_base = ModelBase()
        model_base.orm
        self.assertTrue(mock_db_driver.called)
