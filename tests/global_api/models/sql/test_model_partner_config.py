import mock

from global_api.models.sql.tb_partner_config import PartnerConfig
from tests import BaseTests


class TestModelPartnerConfig(BaseTests):
    @mock.patch('global_api.models.sql.tb_partner_config.ModelBase.orm')
    def test_model_partner_config_return_values(self, mock_model_base_orm):
        partner_config = PartnerConfig()
        partner_config.get_settings()
        self.assertTrue(mock_model_base_orm.db_session.query.called)
