from mock import mock

from global_api.models.nosql import NoSqlDriver
from global_api.models.nosql import Singleton
from global_api.models.nosql.gateway_events_collection import GatewayEvents
from tests import BaseTests


class TestNoSqlDriver(BaseTests):
    def setUp(self):
        Singleton.drop()

    @mock.patch('global_api.models.nosql.connect')
    def test_db_save(self, mock_connect):
        nosql_driver = NoSqlDriver()
        GatewayEvents.save = mock.MagicMock(side_effect=mock.MagicMock())
        nosql_driver.db_save(GatewayEvents, request={'a': 'b', 'b': 'a'}, response={'a': 'b', 'b': 'a'},
                             partner='tim', event='mo', host='http://my-uri.com', status_code=200)
        self.assertTrue(mock_connect.called)
        self.assertTrue(GatewayEvents.save.called)

    @mock.patch('global_api.models.nosql.connect')
    @mock.patch('global_api.models.nosql.gw_logger')
    def test_db_save_error(self, mock_gw_logger, mock_connect):
        nosql_driver = NoSqlDriver()
        mock_model = mock.MagicMock(side_effect=Exception('error'))

        nosql_driver.db_save(mock_model, request={'a': 'b', 'b': 'a'}, response={'a': 'b', 'b': 'a'},
                             partner='tim', event='mo', host='http://my-uri.com', status_code=200)

        self.assertTrue(mock_connect.called)
        self.assertTrue(mock_gw_logger.get.called)
