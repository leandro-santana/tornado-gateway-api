import mock

from global_api import create_app
from tests import BaseTests


class TestCreateApp(BaseTests):
    @mock.patch('global_api.Application')
    def test_create_app(self, mock_application):
        create_app()
        self.assertTrue(mock_application.called)
