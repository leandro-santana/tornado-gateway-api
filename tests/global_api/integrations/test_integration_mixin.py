import json
import uuid
from asyncio import coroutine

import mock
from tornado.testing import gen_test

from global_api.exceptions import InvalidConnection
from global_api.integrations import IntegrationsMixin
from tests import BaseTestsAsync


class ResponseMock(object):
    body = ''


class TestIntegrationMixin(BaseTestsAsync):
    @gen_test
    async def test_process_queue_mode_is_true(self):
        expected_response = {'response': 'queued_success'}

        @coroutine
        def mock_response(*args, **kwargs):
            return expected_response

        payload = json.dumps({
            "carrier": "tim",
            "msisdn": 5511123456789,
            "service_id": "TIM_ENSINA_MENSAL",
            "txid": "dd7464d9-01de-4fab-b6e9-54bc0336cb47",
            "la": 77000,
            "text": "tim - mt generic sync"})

        integration_mixin = IntegrationsMixin()
        integration_mixin.process_in_queue_mode = mock.MagicMock(side_effect=mock_response)
        integration_mixin.normalize_request = mock.MagicMock(True)

        response = await integration_mixin.process(payload=payload, queue_mode=True,
                                                   my_response_coroutine=expected_response)

        self.assertEqual(response, expected_response)

    @gen_test
    async def test_process_queue_mode_is_false(self):
        expected_response = {
            "SendSmsResponse": {
                "message": "ok",
                "result": "SUCCESS",
                "txId": "252458bb-007d-4faa-82ff-aa92e4757e27"
            }}

        @coroutine
        def mock_response(*args, **kwargs):
            return expected_response

        payload = json.dumps({
            "carrier": "tim",
            "msisdn": 5511123456789,
            "service_id": "TIM_ENSINA_MENSAL",
            "txid": "dd7464d9-01de-4fab-b6e9-54bc0336cb47",
            "la": 77000,
            "text": "tim - mt generic sync"})

        integration_mixin = IntegrationsMixin()
        integration_mixin.normalize_request = mock.MagicMock(True)
        integration_mixin.process_in_request_mode = mock.MagicMock(side_effect=mock_response)
        response = await integration_mixin.process(payload=payload, queue_mode=False)

        self.assertEqual(response, expected_response)

    @mock.patch('global_api.integrations.GwRoutes')
    @mock.patch('global_api.integrations.Requests')
    @mock.patch('global_api.integrations.IntegrationsMixin.partner', 'tim')
    @gen_test
    async def test_process_in_request_mode(self, mock_requests, mock_gw_routes):
        @coroutine
        def mock_response_to_request(*args, **kwargs):
            expected_response = json.dumps({"SendSmsResponse": {
                "message": "ok",
                "result": "SUCCESS",
                "txId": "9bddd5df-a0ce-460c-a892-ccdf2ce9df16"
            }})
            ResponseMock.body = expected_response.encode()
            return ResponseMock

        mock_requests().call_request_async = mock.MagicMock(side_effect=mock_response_to_request)
        integration_mixin = IntegrationsMixin()
        response = await integration_mixin.process_in_request_mode('request_fake')
        self.assertTrue(mock_gw_routes.called)
        self.assertTrue(mock_requests.called)
        self.assertIn('SendSmsResponse', response.get('response', {}))

    @mock.patch('global_api.integrations.GwRoutes')
    @mock.patch('global_api.integrations.QueueDriver')
    @gen_test
    async def test_process_in_queue_mode_success(self, mock_queue_driver, mock_gw_routes):
        @coroutine
        def mock_response_to_queue(*args, **kwargs):
            return uuid.uuid4()

        integration_mixin = IntegrationsMixin()
        mock_queue_driver('').send_to_queue = mock.MagicMock(side_effect=mock_response_to_queue)
        response = await integration_mixin.process_in_queue_mode('request_fake')
        self.assertTrue(mock_gw_routes.called)
        self.assertTrue(mock_queue_driver.called)
        self.assertIsNotNone(response.get('queued_success'))

    @mock.patch('global_api.integrations.GwRoutes')
    @mock.patch('global_api.integrations.QueueDriver')
    @gen_test
    async def test_process_in_queue_mode_error(self, mock_queue_driver, mock_gw_routes):
        with self.assertRaises(InvalidConnection) as context:
            @coroutine
            def mock_response_to_queue(*args, **kwargs):
                return None

            integration_mixin = IntegrationsMixin()
            mock_queue_driver('').send_to_queue = mock.MagicMock(side_effect=mock_response_to_queue)
            await integration_mixin.process_in_queue_mode('request_fake')
        self.assertEqual('queued_error', context.exception.message)
        self.assertEqual(400, context.exception.code)
        self.assertTrue(mock_gw_routes.called)
