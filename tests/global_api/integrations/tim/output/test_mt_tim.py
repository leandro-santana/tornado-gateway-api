import mock
from tornado.testing import gen_test

from global_api.integrations.tim.output.mt import TimMt
from tests import BaseTestsAsync


class TestTimMt(BaseTestsAsync):
    def test_class_instance(self):
        tim_mt = TimMt(headers={})
        self.assertEqual(tim_mt.carrier, 'tim')
        self.assertEqual(tim_mt.partner, 'tim')
        self.assertEqual(tim_mt.event, 'mt')
        self.assertEqual(tim_mt.key, 'partner/mt/url')

    @mock.patch('global_api.integrations.tim.output.mt.fields')
    def test_contract(self, mock_fields):
        tim_mt = TimMt(headers={})
        response = tim_mt.contract()
        self.assertTrue(response.get('carrier'))
        self.assertTrue(response.get('la'))
        self.assertTrue(response.get('msisdn'))
        self.assertTrue(response.get('service_id'))
        self.assertTrue(response.get('text'))
        self.assertTrue(response.get('txid'))
        self.assertTrue(mock_fields.Integer.called)
        self.assertTrue(mock_fields.Str.called)

    @gen_test
    async def test_normalize_request(self):
        origin_request = {'msisdn': 5511123456789, 'txid': 'c4951524-48af-4204-9cb4-6bce2fa1f912', 'carrier': 'tim',
                          'text': 'tim - mt generic sync', 'la': 77000, 'service_id': 'TIM_ENSINA_MENSAL'}

        tim_mt = TimMt(headers={})
        response = await tim_mt.normalize_request(origin_request)
        self.assertTrue(response.get('SendSmsRequest'))
        self.assertTrue(response.get('SendSmsRequest', {}).get('serviceId'))
        self.assertTrue(response.get('SendSmsRequest', {}).get('text'))
        self.assertTrue(response.get('SendSmsRequest', {}).get('msisdn'))
        self.assertTrue(response.get('SendSmsRequest', {}).get('txId'))
