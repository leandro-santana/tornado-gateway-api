import mock
from tornado.testing import gen_test

from global_api.integrations.tim.input.mo import TimMo
from tests import BaseTestsAsync


class TestTimMo(BaseTestsAsync):
    def test_class_instance(self):
        tim_mo = TimMo(headers={})
        self.assertEqual(tim_mo.carrier, 'tim')
        self.assertEqual(tim_mo.partner, 'tim')
        self.assertEqual(tim_mo.event, 'mo')
        self.assertEqual(tim_mo.key, 'tim/hub/v1/backend/mo/url')

    @mock.patch('global_api.integrations.tim.input.mo.fields')
    def test_contract(self, mock_fields):
        tim_mo = TimMo(headers={})
        response = tim_mo.contract()
        self.assertTrue(response.get('SmsNotificationRequest'))
        self.assertTrue(mock_fields.Nested.called)

    @gen_test
    async def test_normalize_request(self):
        origin_request = {
            'SmsNotificationRequest': {'shortNumber': 77000, 'token': '456', 'text': 'ajuda', 'msisdn': 5511123456789,
                                       'txId': '67dde825-d0ff-4cac-9b0a-ae4629aa428e'}}
        tim_mo = TimMo(headers={})
        response = await tim_mo.normalize_request(origin_request)
        self.assertTrue(response.get('msisdn'))
        self.assertTrue(response.get('shortNumber'))
        self.assertTrue(response.get('text'))
        self.assertTrue(response.get('token'))
        self.assertTrue(response.get('txId'))
