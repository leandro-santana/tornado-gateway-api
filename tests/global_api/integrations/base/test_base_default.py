import mock

from global_api.integrations.base.base_default import BaseDefault
from tests import BaseTests


class TestBaseDefault(BaseTests):
    @mock.patch.multiple(BaseDefault, __abstractmethods__=set())
    def test_fs_mt_abstract_init_values_validate(self):
        partner = 'timhub'
        key = 'partner/mt/url'
        base_default = BaseDefault(partner=partner, key=key)
        self.assertEqual(base_default.partner, partner)
        self.assertEqual(base_default.key, key)
        self.assertTrue(base_default._header.get('X-Fs-Trackid'))
        self.assertEqual(base_default._header.get('Accept'), 'application/json')
        self.assertEqual(base_default._header.get('Content-Type'), 'application/json')
        self.assertIsNotNone(base_default._header.get('Authorization'))

    @mock.patch.multiple(BaseDefault, __abstractmethods__=set())
    def test_validate_process(self):
        with self.assertRaises(NotImplementedError) as context:
            base_default = BaseDefault()
            base_default.process(request='')
        self.assertTrue('Implement me' in str(context.exception))

    @mock.patch.multiple(BaseDefault, __abstractmethods__=set())
    def test_validate_normalize_request(self):
        with self.assertRaises(NotImplementedError) as context:
            base_default = BaseDefault()
            base_default.normalize_request(origin_request='')
        self.assertTrue('Implement me' in str(context.exception))

    @mock.patch.multiple(BaseDefault, __abstractmethods__=set())
    def test_validate_contract(self):
        with self.assertRaises(NotImplementedError) as context:
            base_default = BaseDefault()
            base_default.contract()
        self.assertTrue('Implement me' in str(context.exception))
