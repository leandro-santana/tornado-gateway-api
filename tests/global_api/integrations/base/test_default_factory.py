import mock
from tornado.web import HTTPError

from global_api.integrations.base.base_factory import BaseDefaultFactory
from tests import BaseTests


class TestDefaultFactory(BaseTests):
    @mock.patch('global_api.integrations.base.base_factory.BaseDefault')
    def test_get_instance_return_http_error(self, mock_mo_abstract):
        with self.assertRaises(HTTPError) as context:
            mock_mo_abstract.__class__.__subclasses__ = mock.MagicMock(return_value=[])
            BaseDefaultFactory.get_instance(carrier='nothing', event='nothing')

        self.assertTrue('HTTP 404: Carrier not found' in str(context.exception))

    def test_get_instance_return_class(self):
        instance = BaseDefaultFactory.get_instance(carrier='tim', event='mt')
        self.assertEqual(instance.carrier, 'tim')
        self.assertEqual(instance.event, 'mt')
        self.assertEqual(instance.key, 'partner/mt/url')
        self.assertEqual(instance.partner, 'tim')
        self.assertTrue(hasattr(instance, '_header'))
