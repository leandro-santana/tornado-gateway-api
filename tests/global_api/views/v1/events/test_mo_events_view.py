import json

import mock
from tornado.httpclient import HTTPError

from global_api.exceptions import InvalidConnection
from global_api.views.v1.events.mo_events_view import MoEventsView
from tests import BaseAsyncHttpTestCase


class Instance:
    @staticmethod
    def process(request):
        return request


class TestMTEventsView(BaseAsyncHttpTestCase):
    @mock.patch('global_api.views.v1.events.mo_events_view.NoSqlDriver')
    @mock.patch('global_api.views.v1.events.mo_events_view.json')
    @mock.patch('global_api.views.v1.events.mo_events_view.BaseDefaultFactory')
    @mock.patch('global_api.views.v1.events.mo_events_view.parser')
    def test_http_post_success(self, mock_base_default_factory, mock_parser, mock_json, mock_no_sql_driver):
        MoEventsView.request = mock.MagicMock(return_value={})
        mock_base_default_factory.get_instance = mock.MagicMock(return_value=Instance)
        response = self.fetch('/tim/hub/mo', raise_error=False, method="POST", body=json.dumps({'carrier': 'tim'}))
        self.assertEqual(200, response.code)
        self.assertIn('status', json.loads(response.body.decode()))
        self.assertIn('message', json.loads(response.body.decode()))
        self.assertTrue(mock_parser.get_instance.called)
        self.assertTrue(mock_json.dumps.called)
        self.assertTrue(mock_json.loads.called)
        self.assertTrue(mock_no_sql_driver.called)

    @mock.patch('global_api.views.v1.events.mo_events_view.BaseDefaultFactory')
    @mock.patch('global_api.views.v1.events.mo_events_view.json')
    def test_http_post_with_http_error(self, mock_json, mock_base_default_factory):
        error_code = 422
        error_message = 'http_error'
        mock_base_default_factory.get_instance().process.side_effect = HTTPError(error_code, error_message)
        with self.assertRaises(Exception) as context:
            self.fetch('/tim/hub/mo', raise_error=True, method="POST", body=json.dumps({'carrier': 'tim'}))
        self.assertEqual(error_code, context.exception.response.code)
        self.assertEqual('{"message": "http_error", "status": "error"}', context.exception.response.body.decode())
        self.assertEqual(context.exception.response.error.message, 'Unprocessable Entity')
        self.assertTrue(mock_json.dumps.called)

    @mock.patch('global_api.views.v1.events.mo_events_view.BaseDefaultFactory')
    @mock.patch('global_api.views.v1.events.mo_events_view.json')
    def test_http_post_with_common_errors(self, mock_json, mock_base_default_factory):
        error_code = 404
        error_message = 'ROUTENOTFOUND'
        mock_base_default_factory.get_instance().process.side_effect = InvalidConnection(message=error_message,
                                                                                         code=error_code)
        with self.assertRaises(Exception) as context:
            self.fetch('/tim/hub/mo', raise_error=True, method="POST", body='')
        self.assertEqual(error_code, context.exception.response.code)
        self.assertEqual('{"message": "ROUTENOTFOUND", "status": "error"}', context.exception.response.body.decode())
        self.assertTrue(mock_base_default_factory.get_instance.called)
        self.assertTrue(mock_json.dumps.called)
