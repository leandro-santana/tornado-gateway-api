import mock


from global_api.models.sql.tb_partner import Partner
from global_api.models.sql.tb_partner_config import PartnerConfig
from global_api.services import SingletonComplexity
from global_api.services.gateway import GwRoutes
from tests import BaseTests


class Testglobal_api(BaseTests):
    def setUp(self):
        SingletonComplexity.drop()

    @mock.patch('global_api.services.gateway.CacheDriver')
    def test_get_gw_routes_is_cache_not_expires(self, mock_cache_driver):
        config_routes = {'a': 1}
        mock_cache_driver().get.return_value = 999
        gwroutes = GwRoutes()
        gwroutes.routes = config_routes
        configs = gwroutes.open()

        self.assertTrue(mock_cache_driver.called)
        self.assertDictEqual(configs, config_routes, 'not compare')

    @mock.patch('global_api.services.gateway.CacheDriver')
    @mock.patch('global_api.models.sql.tb_partner_config.PartnerConfig.get_settings')
    def test_get_gw_routes_is_cache_expires(self, mock_get_settings, mock_cache_driver):
        gw_routes = GwRoutes()
        gw_routes.open()
        self.assertTrue(mock_get_settings.called)
        self.assertTrue(mock_cache_driver.called)

    @mock.patch('global_api.services.gateway.CacheDriver')
    @mock.patch('global_api.models.sql.tb_partner_config.PartnerConfig.get_settings')
    def test_get_gw_routes_is_cache_expires_and_get_settings_with_active(self, mock_get_settings, mock_cache_driver):
        dict_compare = {'timhub': {
            'interactivity/la': '[77000,22020]',
            'partner/mt/url': 'http://globalsdp-mock-homol.whitelabel.com.br/tim_hub/from_backend'}}

        mock_get_settings.return_value = [
            [

                Partner(name='TimHub', id=8, actived=1,
                        default_url='http://globalsdp-mock-homol.whitelabel.com.br/tim_hub'),
                PartnerConfig(id=4, partner_id=8, value='[77000,22020]', key='interactivity/la', actived=1)
            ], [
                Partner(name='TimHub', id=8, actived=1,
                        default_url='http://globalsdp-mock-homol.whitelabel.com.br/tim_hub'),
                PartnerConfig(id=2, partner_id=8, value='from_backend', key='partner/mt/url', actived=1)
            ]
        ]
        gw_routes = GwRoutes()
        response = gw_routes.open()

        self.assertDictEqual(response, dict_compare, 'not compare')
        self.assertTrue(mock_cache_driver.called)
