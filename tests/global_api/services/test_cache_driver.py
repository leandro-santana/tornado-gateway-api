import mock

from global_api.services import SingletonComplexity
from global_api.services.cache_driver import CacheDriver
from tests import BaseTests


class TestCacheDriver(BaseTests):
    def setUp(self):
        SingletonComplexity.drop()

    @mock.patch('global_api.services.cache_driver.redis.StrictRedis')
    def test_set_cache_not_null(self, mock_redis):
        cache = CacheDriver(p_host='teste', p_port='6379', p_db=0)
        cache.set('a key', 'a value')
        self.assertTrue(cache._CacheDriver__redis.set.called)
        self.assertTrue(mock_redis.called)

    @mock.patch('global_api.services.cache_driver.redis.StrictRedis')
    def test_get_cache_not_null_value(self, mock_redis):
        a_key = 'key'
        a_value = b'a value'

        mock_redis().get.return_value = a_value
        cache = CacheDriver(p_host='teste', p_port='6379', p_db=0)

        result = cache.get(a_key)
        self.assertEqual(result, a_value.decode())

    @mock.patch('global_api.services.cache_driver.redis.StrictRedis')
    def test_get_cache_null_value(self, mock_redis):
        a_key = 'key'
        a_value = None

        mock_redis().get.return_value = a_value
        cache = CacheDriver(p_host='teste', p_port='6379', p_db=0)

        result = cache.get(a_key)
        self.assertEqual(result, a_value)

    @mock.patch('global_api.services.cache_driver.redis.StrictRedis')
    def test_delete_cache(self, mock_redis):
        a_key = 'a key'
        cache = CacheDriver(p_host='teste', p_port='6379', p_db=0)

        cache.delete(a_key)
        self.assertTrue(mock_redis().delete.called)

    @mock.patch('global_api.services.cache_driver.redis.StrictRedis')
    def test_flushall(self, mock_redis):
        cache = CacheDriver(p_host='teste', p_port='6379', p_db=0)
        cache.flushall()
        self.assertTrue(mock_redis().flushall.called)
