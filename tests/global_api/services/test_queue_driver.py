import uuid

import mock
from tornado.testing import gen_test

from global_api.services.queue_driver import QueueDriver
from global_api.tasks.gateway import worker_job_request
from tests import BaseTestsAsync


class TestQueueDriver(BaseTestsAsync):
    @gen_test
    async def test_send_to_queue_sucess(self):
        uuid_v4 = str(uuid.uuid4())
        worker_job_request.apply_async = mock.MagicMock(return_value=uuid_v4)
        queue_driver = QueueDriver('worker_job_request')
        response = await queue_driver.send_to_queue(message='test_to_queue', queue='no_queue', priority=1)
        self.assertTrue(queue_driver.list_jobs_queues.get('worker_job_request').apply_async.called)
        self.assertEqual(response, uuid_v4)

    def test_get_instance_queue_not_exists(self):
        job_queue = 'queue_not_exist'
        with self.assertRaises(Exception) as context:
            QueueDriver(job_queue)
        self.assertEqual('has not queue {queue_job}'.format(queue_job=job_queue), str(context.exception))
