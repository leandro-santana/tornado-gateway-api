import mock
from tornado.testing import AsyncHTTPTestCase, AsyncTestCase, unittest

from global_api import create_app


class BaseAsyncAwait:
    @staticmethod
    def mock_await():
        async def async_magic():
            pass

        mock.MagicMock.__await__ = lambda x: async_magic().__await__()


class BaseAsyncHttpTestCase(AsyncHTTPTestCase, BaseAsyncAwait):
    def get_app(self):
        return create_app()

    def setUp(self):
        self.mock_await()
        super().setUp()

    def tearDown(self):
        super().tearDown()


class BaseTestsAsync(AsyncTestCase, BaseAsyncAwait):
    def setUp(self):
        self.mock_await()
        super().setUp()

    def tearDown(self):
        super().tearDown()


class BaseTests(unittest.TestCase):
    pass
