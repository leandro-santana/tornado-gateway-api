import json
import requests

import asyncio
import multiprocessing
from concurrent.futures import ThreadPoolExecutor

cpu_count = multiprocessing.cpu_count()


async def transform_function_future(p_func, *args):
    loop = asyncio.get_event_loop()
    executor = ThreadPoolExecutor(
        max_workers=cpu_count,
    )
    return await loop.run_in_executor(executor, p_func, *args)


def request_token(token, url):

    response_ = requests.post(url, headers={"Authorization": "Basic %s" % token},
                              data={'grant_type': 'client_credentials'})

    return json.loads(response_.text)


async def calling_all():
    token = ''
    url = '{}/token'.format('https://apigateway.serpro.gov.br')
    return await transform_function_future(request_token, token, url)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    response = loop.run_until_complete(calling_all())
    loop.close()
    print(response)
